[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4687295.svg)](https://doi.org/10.5281/zenodo.4687295)

# ground_conductivity

Input raster file using for computing the shallow energy geothermal potential (conductivity, ground temperature and seasonal length) according to the G.pot methodology (https://www.sciencedirect.com/science/article/pii/S0360544216303358)

## Repository structure

```
datapackage.json                       -- Datapackage JSON file with themain meta-data
data/ground_conductivity.tif           -- Raster with the ground conductivity [W/m/K]
```

## Description of the task

The present task provides data with following characteristics:

<table>
  <tr>
    <td> </td>
    <td>Spatial resolution</td>
    <td>Temporal resolution</td>
  </tr>
  <tr>
    <td>Shallow geothermal</td>
    <td>EU28</td>
    <td>–</td>
  </tr>
</table>

**Table 1.** Characteristics of data provided within Task 2.6 Renewable energy sources data collection and potential review.

The repository provide the ground_conductivity [W/m/K] layer that can be used to assess the Shallow Geothermal Potential.

The ground conductivity is derived from the shape file of [Thermomap project](http://www.eurogeosurveys.org/projects/thermomap/), that are available in this  [repository](https://gitlab.com/hotmaps/potential/potential_shallowgeothermal/tree/master/data). In particular the shape file has been converted using the following [GRASS GIS](https://grass.osgeo.org/) command: [v.to.rast](https://grass.osgeo.org/grass78/manuals/v.to.rast.html).

```bash
$ v.to.rast input="thermomap" layer=1 type="point,line, area" \
            output="ground_conductivity" \
            use="attr" attribute_column="head_cond"  \
            memory=4096
```

The computationa region used is:

```bash
$ g.region -p
projection: 99 (ETRS89 / LAEA Europe)
zone:       0
datum:      etrs89
ellipsoid:  grs80
north:      5414000
south:      938000
west:       944000
east:       6528000
nsres:      100
ewres:      100
rows:       44760
cols:       55840
cells:      2499398400
```


## How to cite
Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Müller (e‐think), Michael Hartner (TUW), Tobias Fleiter, Anna‐Lena Klingler, Matthias Kühnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW) Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 www.hotmaps-project.eu

## Authors

Pietro Zambelli<sup>*</sup>.


<sup>*</sup> Eurac Research 

Institute for Renewable Energy
VoltaStraße/Via Via A. Volta 13/A
39100 Bozen/Bolzano

## License

Pietro Zambelli <pietro.zambelli@eurac.edu>
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0
License-Text: https://spdx.org/licenses/CC-BY-4.0.html


## Acknowledgement

We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.
